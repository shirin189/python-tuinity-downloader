import os
import json
import requests as req

def getCiData():
    apiget = req.get('https://ci.codemc.io/job/Spottedleaf/job/Tuinity/api/json?pretty=true')
    data = apiget.json()
    builds = data["builds"]
    latest = builds[0]
    jsd = json.dumps(latest)
    return latest

def downloadTuinity(link):
    fullUrl = link + "artifact/tuinity-paperclip.jar"
    file = req.get(fullUrl)
    open('tuinity-paperclip.jar','wb').write(file.content)
    print('File Downloaded! Exiting')

def writeFile(ver):
    f = open('ver.txt','w')
    verStr = str(ver)
    f.write(verStr)
    f.close

def checkUpdate(data):
    link = data["url"]
    ver = data["number"]
    f = open('ver.txt', 'r')
    cur = f.read()
    f.close()
    cur_int = int(cur)
    if cur_int < ver :
        print('new update detected, downloading new update')
        writeFile(ver)
        downloadTuinity(link)
    else:
        print('Tuinity is up to date! exiting')


def checkVersion(data):
    link = data["url"]
    if os.path.exists('ver.txt'):
        print('file exist, checking for update')
        checkUpdate(data)
    else:
        print('file doesnt exist, creating one and redownloading tuinity')
        writeFile(data["number"])
        os.remove('tuinity-paperclip.jar')
        downloadTuinity(link)


def checkTuinity():
    latest = getCiData()
    if os.path.exists('tuinity-paperclip.jar'):
        print('tuinity exists')
        checkVersion(latest)
    else:
        print('tuinity not exists, downloading latest from CI')
        writeFile(latest["number"])
        downloadTuinity(latest["url"])

def debugPring():
    data = getCiData()
    print(data)

checkTuinity()

