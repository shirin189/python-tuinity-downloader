# Python Tuinity Downloader
just simple script that i made using python
this script checks for `tuinity-paperclip.jar` and `ver.txt` for version logging
if none of them exist, this script will download latest tuinity and create ver.txt
if only tuinity exist, this script will delete and redownload tuinity to make sure it's using latest version

## Dependencies
    - Python 3
    - requests.py

## Usage
    - Download all requirements `pip install -r requirements.txt`
    - Run script `python3 updater.py`

## TODO
    - [ ] Refactor code
